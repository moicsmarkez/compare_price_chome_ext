
chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab){
    if (changeInfo.status == 'complete' && tab.status == 'complete' && tab.url != undefined) {
        checkDomain(tab);
    }
});

chrome.runtime.onMessage.addListener(function(message, sender) {
    if (message.sendBack === true) {
        if (sender !==undefined && sender.tab.id!==undefined) {
            chrome.tabs.sendMessage(sender.tab.id, {method: "closeFrame"});
        }else {
            return;
        }
    }
});

function checkDomain(tab){
    var meUrL = tab.url, docTitle = tab.title,reg1,claveTitulo;
    var link = document.createElement('a');
    link.href = tab.url;
    
    if(!link.pathname || link.pathname.length < 2 || /(stackoverflow)/i.test(link.href)){
                    return;
    }else if (/(\/((product)|(itm)|(p)|(acatalog)|(catalog)|(store)|(dp)|(gp))\/)|productid=|[\/\?]produs[\/=]/i.test(meUrL) && !/http[s]?:\/\/(www)?fares\.ro/i.test(meUrL) && !/produ(s|ctid)=($|&)/i.test(meUrL) && !/image/i.test(meUrL)) {
                    getMurphysProducts(link, tab);
					return;
				}
    else if(/(chrome:\/\/extensions)/gi.test(meUrL)){
                //no hacer un carajo
                return;
               }
    else{
                chrome.tabs.sendMessage(tab.id, {method: "detectionWhitoutPagePathname"},(function(response) {
                    if( response.method == "detectionWhitoutPagePathname" ){
                        if(response.data == true){
                            getMurphysProducts(link, tab);
                            return;
                        }
                    }
                }));
                return;
            }
 }

 function getMurphysProducts(link,tab){
    var tituloT, precioP, monedaM;
    chrome.tabs.sendMessage(tab.id, {method: "getText"}, function(response) {
                    if(response.method=="getText"){
                        tituloT = response.titulo_t;
                        precioP = response.precio_p;
                        monedaM = response.moneda_m;
                    }else{
                        chrome.pageAction.hide(tab.id);
                    }
                    
                    if(/(\£)/gi.test(monedaM)){
                        precioP = (precioP * 1.29449).toFixed(2);
                    }else if(/(\€)/gi.test(monedaM)){
                        precioP = (precioP * 1.12330).toFixed(2);
                    }
                    var url = 'http://52.54.237.11:9090/ScraperMagic/rest/json/requiero/dos/'+tituloT;
                    var xhr=new XMLHttpRequest();
                    var objeto_Json;
                    xhr.open('GET', url, true);
                    xhr.onreadystatechange=function(){
                        if(xhr.readyState == 4  && xhr.status == 200){
                            objeto_Json= JSON.parse(xhr.responseText);    
                            if(objeto_Json.p_t=='true'){
                                if(parseFloat(objeto_Json.precio_mph) < parseFloat(precioP)){
                                    chrome.pageAction.show(tab.id);
                                    chrome.notifications.create("mddshIDnt132121"+tab.id,{type: "basic",title: "Magicians' Discount Shopping Club - We offer discounts:",message: "You can save a " + (((precioP-objeto_Json.precio_mph)*100)/precioP).toFixed(0) +"%  - Click Here",iconUrl: "img/defaultIcon19x19.png"});
                                    chrome.notifications.onClicked.addListener(function (){
                                        chrome. tabs.update(tab.id, {active: true});
                                        chrome.notifications.clear("mddshIDnt132121"+tab.id);
                                        chrome.windows.update(chrome.windows.WINDOW_ID_CURRENT, {focused: true});
                                    });
                                    chrome.tabs.executeScript(tab.id, {file:'js/app/ventanita.js'}, function () {
                                        chrome.tabs.sendMessage(tab.id, {precioP: precioP, objeto_Json: objeto_Json});
                                    });
                                    chrome.pageAction.setPopup({tabId: tab.id, popup: "popup.html#?titulo="+objeto_Json.titulo.replace(/\#/gi,'')+"&imagen_url="+objeto_Json.imagen_url+"&precio="+precioP+"&precio_mph="+objeto_Json.precio_mph+"&closable="+false+"&productid="+objeto_Json.code_id});
                                }else{
                                    chrome.pageAction.hide(tab.id);
                                }
                            }else {
                                chrome.pageAction.hide(tab.id);
                            }
                        }else{
                            chrome.pageAction.hide(tab.id);
                        }
                    }
                    xhr.send(null);
                });     
}





    

