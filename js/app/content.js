﻿var resultados_price=[];
function Probador() {
    this.pruebaDepurada = {};
}
                
        function sinPrecio(arrayNodes) {
            for(var i=0; i < arrayNodes.length; i++){
                if(arrayNodes[i].nodeType == 1 && ((arrayNodes[i].tagName.toLowerCase() == 'aside' || /(product-related)/gi.test(arrayNodes[i].className)) || /(product-related)/gi.test(arrayNodes[i].id))) {
                  continue;
                }else if (arrayNodes[i].tagName.toLowerCase()!='strike' && arrayNodes[i].tagName.toLowerCase()!='script'){
                    if(!(/(retail)|(old)|(new)|(save)|(member)/i.test(arrayNodes[i].className))&&!(/(retail)|(old)|(new)|(save)|(member)/i.test(arrayNodes[i].id))) {
                        if(arrayNodes[i].nodeType ==1 && (/(price)/gi.test(arrayNodes[i].className) || /(price)/gi.test(arrayNodes[i].id))){
                            moneda = arrayNodes[i].innerText.match(/(\$)|(\€)|(\£)/gi);
                            precio = arrayNodes[i].innerText.match(/((?:\d|(\.|\,))*(\.|,)?\d+)/gi);
                            //alert('Precio Sin precio: '+moneda + precio + "  "+iterateWalkEnd(arrayNodes[i]));
                            resultados_price.push({el: arrayNodes[i], moneda: moneda, precio: precio, puntos_css: 50, puntos: (iterateWalkEnd(arrayNodes[i])/10)});
                        }
                    }
                }
            }
        }
        
        
        
        function burcarNodosPrecioXhijos(n){
            var hijo=n.firstChild, moneda, precio;
            for (;hijo!=null;hijo=hijo.nextSibling){
                if(hijo.nodeType == 1 && ((hijo.tagName.toLowerCase() == 'aside' || /(product-related)/gi.test(hijo.className)) || /(product-related)/gi.test(hijo.id))) {
//                     alert("tengo que esquivar este:"+hijo.innerText);
                    continue;
                }else if(hijo.nodeType == 3){
                    if( (/(\$|\€|\£)\s?((?:\d|(\.|\,))*(\.|,)?\d+)/gi.test(hijo.nodeValue)) || (/((?:\d|(\.|\,))*(\.|,)?\d+)\s?(\$|\€|\£)/gi.test(hijo.nodeValue)) ){
                        if( hijo.parentNode.tagName.toLowerCase()!='strike' && hijo.parentNode.tagName.toLowerCase()!='script'){
                            if(!(/(retail)|(old)|(new)|(save)|(member)/i.test(hijo.parentNode.className))&&!(/(retail)|(old)|(new)|(save)|(member)/i.test(hijo.parentNode.id))){
                                if(hijo.parentNode.nodeType ==1 && (/(price)/gi.test(hijo.parentNode.className) || /(price)/gi.test(hijo.parentNode.id)) ){                                
                                    moneda = hijo.nodeValue.match(/(\$)|(\€)|(\£)/gi);
                                    precio = hijo.nodeValue.match(/((?:\d|(\.|\,))*(\.|,)?\d+)/gi);
                                    //alert('Precio1: '+moneda + precio + "  "+iterateWalkEnd(hijo.parentNode));
                                    resultados_price.push({el: hijo.parentNode, moneda: moneda, precio: precio, puntos_css: 50, puntos: (iterateWalkEnd(hijo.parentNode)/10)});
                                }else {
                                    moneda = hijo.nodeValue.match(/(\$)|(\€)|(\£)/gi);
                                    precio = hijo.nodeValue.match(/((?:\d|(\.|\,))*(\.|,)?\d+)/gi);
                                    //alert('Precio2: '+moneda + precio);
                                    resultados_price.push({el: hijo.parentNode, moneda: moneda, precio: precio, puntos_css: 0,  puntos: (iterateWalkEnd(hijo.parentNode)/10)});
                                }
                            }
                        }
                    }
                }
                burcarNodosPrecioXhijos(hijo);
            }
        }


		function compararPalabras(titulo, cadena) {
            	var palabras = separadorPal(titulo),
					i = 0,
					l = palabras.length,
					palabra, reg, cont_pal, cont = 0,
					ac;
                    
				for (; i < l; ++i) {
					palabra = palabras[i];
					reg = new RegExp(preg_quote(palabra), 'ig');
                    ac = cadena.match(reg);
                  	cont_pal = ac && ac.length || 0;
                   if (cont_pal) {
						cont++;
					}
				}
				
				return (cont / l * 100);
			}
			/** @desc Split a string into an array of words
			 **/
			function separadorPal(cadena) {
				return (cadena || '').match(/[^\s.,!?\-]+/g) || [];
			}


/** @copyright http://phpjs.org/functions/preg_quote/ **/
			function preg_quote(str, delimiter) {
				return String(str).replace(new RegExp('[.\\\\+*?\\[\\^\\]$(){}=!<>|:\\' + (delimiter || '') + '-]', 'g'), '\\$&');
			}

			function detectonMethod(){
                var datos = [], 
                documento = document.body,
                elementosH1 = documento.getElementsByTagName('h1'),
                elementosH2 = documento.getElementsByTagName('h2'), 
                elementosB = documento.getElementsByTagName('b'), 
                elementosStrong = documento.getElementsByTagName('strong'), 
                puntos=0, valor='', 
                titulo_doc = $.trim(document.title).replace(/\s+/g,' ');
                
                  [].forEach.call(elementosH1, function( el ) {
                     valor = $.trim(el.innerText);
                     puntos = compararPalabras(titulo_doc, valor);
                     
                 if(valor!==''){
                     datos.push({
                         el: el,
                         valor: valor,
                         puntos: puntos
                     });
                 }
             });
            
            [].forEach.call(elementosH2, function( el ) {
                    valor = $.trim(el.innerText);
                    puntos = compararPalabras(titulo_doc, valor);
                    
                if(valor!==''){
                    datos.push({
                        el: el,
                        valor: valor,
                        puntos: puntos
                    });
                }
            });
            
            [].forEach.call(elementosB, function( el ) {
                    valor = $.trim(el.innerText);
                    puntos = compararPalabras(titulo_doc, valor);
                    
                if(valor!==''){
                    datos.push({
                        el: el,
                        valor: valor,
                        puntos: puntos
                    });
                }
            });
            
            if(datos.puntos && datos.puntos < 60){
                [].forEach.call(elementosStrong, function( el ) {
                        valor = $.trim(el.innerText);
                        puntos = compararPalabras(titulo_doc, valor);
                        
                    if(valor!==''){
                        datos.push({
                            el: el,
                            valor: valor,
                            puntos: puntos
                        });
                    }
                });
            }
            
            datos.sort(function (a, b) {
								if (a.puntos > b.puntos) {
									return -1;
								}
								else if (a.puntos < b.puntos) {
									return 1;
								}
								else {
									return 0;
								}
							});
            
            return datos;
            }

			function microdataMethod(){
                var producto = $('*').items('http://schema.org/Product'), 
                datos = $.map(producto.microdata(false), function(value, clave){
                    var monedaC, precioC, nombreC;
                    if(value.type && /(Product)/gi.test(value.type)){
                        puntos = 100;
                        if(/(Details)|(about)/gi.test(value.name)){
                            nombreC = value.name.replace(/(Details)|(about)/gi,'');
                        }
                        if (value.offers != undefined) {
                            if(value.offers.price){
                                precioC = /(\$)|(\€)|(\£)/gi.test(value.offers.price) ? value.offers.price.match(/((?:\d|(\.|\,))*(\.|,)?\d+)/gi) : value.offers.price;
                            }
                            
                            if(value.offers.priceCurrency=='USD' || value.offers.priceCurrency=='$'){
                                monedaC='$';
                            }else if (value.offers.priceCurrency=='EUR' || value.offers.priceCurrency=='€'){
                                monedaC='€';
                            }else if (value.offers.priceCurrency=='GBP' || value.offers.priceCurrency=='£'){
                                monedaC='£';
                            }
                        
                            else if(!monedaC){
                                if(!value.offers.price){
                                        burcarNodosPrecioXhijos(document.body);
                                        resultados_price.sort(function (a, b) {
                                            if(a.puntos_css > 0 ){
                                                a.puntos = a.puntos + a.puntos_css;
                                            }else if(b.puntos_css > 0){
                                                b.puntos = b.puntos + b.puntos_css;
                                            }
                                            if (a.puntos > b.puntos) {
                                                return -1;
                                            }
                                            else if (a.puntos < b.puntos) {
                                                return 1;
                                            }
                                            else {
                                                return 0;
                                            }
                                    });
                                    //alert(resultados_price[0].moneda);
                                    monedaC = resultados_price[0].moneda;
                                    precioC = resultados_price[0].precio;
                                }else {
                                    monedaC = value.offers.price.match(/(\$)|(\€)|(\£)/gi);
                                }
                            }
                        resultados_price.push({el: null, moneda: monedaC, precio:  precioC});
                        }
                        return [{itemtype: value.type,valor: /(Details)|(about)/gi.test(value.name) ? nombreC : value.name,puntos: puntos}];
                      }
                })[0];
                return datos;
            }
			
            function iterateWalkEnd(ch) {
                var cuenta = 0;
                
                while (ch) {
                    if (ch.children.length > 0) {
                        ch = ch.firstElementChild;
                        cuenta++;
                    } else if (ch.nextElementSibling) {
                        ch = ch.nextElementSibling;
                        cuenta++;
                    } else {
                        do {
                            ch = ch.parentNode;
                            if (ch === document.body) {
                                return cuenta;
                            }
                        } while (!ch.nextElementSibling)
                        ch = ch.nextElementSibling;
                    }
                }
            }   

            function porcentajeXprecio(valor1, valor2){
                if(valor1 > valor2){
                    valor1 = valor1 - valor2;
                    valor1 = 100 - ((valor1 * 100)/5);
                    if(valor1 > 5){
                        return 0;
                    }
                    return valor1/2;
                }else if( valor2 > valor1){
                    valor1 = valor2 - valor1;
                    valor1 = 100 - ((valor1 * 100)/5);
                    if(valor1 > 5){
                        return 0;
                    }
                    return valor1/2;
                }
                return 50;
            }
            
    var fAgr = ["add", "place", "\\u0061\\u00f1\\u0061\\u0064\\u0069\\u0072", "\\u0434\\u043e\\u0431\\u0430\\u0432\\u0438", "\\u0434\\u043e\\u0431\\u0430\\u0432\\u0438\\u0442\\u044c", "\\u0068\\u006f\\u007a\\u007a\\u00e1\\u0061\\u0064\\u00e1\\u0073"],
    rAgr = new RegExp('(?:^|\\s|_|-)(?:' + fAgr.join('|') + ')(?:$|\\s|_|-)', 'i'),
    fCarro = ["bag", "cart", "basket", "co[s\\u015f\\u0219]", "\\u043a\\u043e\\u043b\\u0438\\u0447\\u043a\\u0430", "\\u006b\\u006f\\u0161\\u00ed\\u006b", "\\u043a\\u043e\\u0440\\u0437\\u0438\\u043d\\u0430", "\\u006b\\u006f\\u0073\\u00e1\\u0072", "\\u30ab\\u30fc\\u30c8", "\\u006b\\u006f\\u0161\\u00ed\\u006b"],
    rCarro = new RegExp(fCarro.join('|'), 'i'),
    fAgr2Carro = ["add[\\s\\-_]?(?:to[\\s\\-_]?)?(?:shop(?:ping)?[\\s\\-_]?)?(?:bag|cart|basket|car)", "shop(?:ping)?[\\s\\-_]?(?:bag|cart|basket|car)", "(?:b(?:ut)?t(?:o)?n[\\s\\-_]?)(?:(?:buy[\\s\\-_]?(?:now)?)|(?:atc))", "(?:(?:buy[\\s\\-_]?(?:now)?)|(?:atc))[\\s\\-_]?(?:b(?:ut)?t(?:o)?n)", "buy[\\s\\-_]?now", "bu?t?to?n[\\s\\-_]?(?:(?:(?:add[\\s\\-_]?)?(?:to[\\s\\-_]?)?cart)|comm?and)", "\\u30ab\\u30fc\\u30c8\\u306b\\u5165\\u308c\\u308b", "\\u0434\\u043e\\u0431\\u0430\\u0432\\u0438\\u0442\\u044c[\\s\\-_]?\\u0432[\\s\\-_]?\\u043a\\u043e\\u0440\\u0437\\u0438\\u043d\\u0443", "\\u0434\\u043e\\u0431\\u0430\\u0432\\u0438[\\s\\-_]?\\u0432[\\s\\-_]?\\u043a\\u043e\\u043b\\u0438\\u0447\\u043a\\u0430\\u0442\\u0430", "\\u30ab\\u30fc\\u30c8\\u306b\\u8ffd\\u52a0", "\\u30ab\\u30fc\\u30c8\\u306b\\u5165\\u308c\\u308b", "\\u30ab\\u30fc\\u30c8\\u3078\\u5165\\u308c\\u308b", "\\u8cb7\\u3044\\u7269\\u304b\\u3054\\u306b\\u5165\\u308c\\u308b", "^\\u653e\\u5165\\u8d2d\\u7269\\u8f66$", "^\\u6dfb\\u52a0\\u5230\\u8d2d\\u7269\\u8f66$", "^\\u8d2d\\u4e70$", "^\\u7acb\\u5373\\u62a2\\u8d2d$", "^\\u7acb\\u5373\\u8d2d\\u4e70$", "^\\u52a0\\u5165\\u8d2d\\u7269\\u8f66$"],
    rAgr2Carro = new RegExp('(?:' + fAgr2Carro.join(')|(?:') + ')', 'i'),
    fComp = ["buy", "order", "\\u006b\\u0443\\u043f\\u0438", "\\u043a\\u0443\\u043f\\u0438", "\\u006b\\u0443\\u043f\\u0438\\u0442\\u044c", "\\u043a\\u0443\\u043f\\u0438\\u0442\\u044c", "\\u043a\\u0443\\u043f\\u0443\\u0432\\u0430\\u043c", "\\u006b\\u00f6\\u0070", "\\u006b\\u00fa\\u0070\\u0069\\u0165", "\\u0073\\u0061\\u0074\\u0131\\u006e\\u0020\\u0061\\u006c"],
    rComp = new RegExp('(?:^|\\s|_|-|\/)(?:' + fComp.join('|') + ')(?:$|\\s|_|-|\/)', 'i'),
    fFComp = ["now", "online", "\\u0441\\u0435\\u0433\\u0430"],
    rFComp = new RegExp(fFComp.join('|'), 'i'),
    rComYa = new RegExp('(?:^|\\s|_|-)(?:' + fComp.join('|') + ')(?:\\s|_|-)?(?:' + fFComp.join('|') + ')(?:$|\\s|_|-)', 'i'),
    fPrOrd = ["pre[\\s\\-_]?order"],
    rPrOrd = new RegExp(fPrOrd.join('|'), 'i'),
    esCadena = function(obj) {
        return Object.prototype.toString.call(obj) === '[object String]';
    };

             
			var PrbaLazndo = Probador.prototype;
            
			PrbaLazndo.empzDepu = function (tipete, prbaS, prbaSNmbrs) {
				var prbIden = ((new Date().getTime() + Math.random()) / Math.random() * 10000).toString(32),
					prba = this.obtenPrbStar(prbaS, (tipete === 'AND') ? true : false, false ? prbIden : null),
					b, prb = this;
				if (false) {
					b = this.pruebaDepurada[prbIden] = {};
					return function (node, data) {
						b = prb.pruebaDepurada[prbIden] = {
							prba: prba,
							tipo: tipete
						};
						var prbComplta = d.result = prba(node, data),
							z = (prbComplta) ? b.success : b.fail,
							voiddID = (prbaSNmbrs && prbaSNmbrs[z.id]) ? ('(' + z.id + ') ' + prbaSNmbrs[z.id]) : z.id,
							voiddNbre = String(z.fn).match(/'!name:(\w+)!';/)[1];
						b.voiddID = voiddID;
						b.voiddID = voiddNbre;
						return prbComplta;
					}
				}
				else {
					return prba;
				}
			};
            
            PrbaLazndo.CAS_pasdres = function (prba, padreMaximos, guardaRefNombres) {
				return function (nodo, dato) {
					'!name:CAS_pasdres!';
					var nodoActual = nodo;
					var i = 0;
					while (nodoActual && i <= padreMaximos) {
						if (prba(nodoActual, {})) {
							if (guardaRefNombres != null && dato != null) {
								dato[guardaRefNombres] = nodoActual;
							}
							return true;
						}
						nodoActual = nodoActual.parentNode;
						i++;
					}
					return false;
				};
			};
			
			PrbaLazndo.obtenPrbStar = function (prbas, ajaY, dpeuID) {
				var prb = this;
				if (ajaY == null) {
					ajaY = true;
				}
				return function (nodo, dato) {
					'!name:obtenPrbStar!';
					var prba;
					for (var t = 0, len = prbas.length; t < len; ++t) {
						prba = prbas[t];
						if (prba(nodo, dato)) {
							if (!ajaY) {
								if (dpeuID) {
									prb.pruebaDepurada[dpeuID]['success'] = {
										id: t,
										voidd: prba,
										nodo: nodo,
										dato: dato
									};
								}
								return true;
							}
						}
						else {
							if (ajaY) {
								if (dpeuID) {
									prb.pruebaDepurada[dpeuID]['fail'] = {
										id: t,
										voidd: prba,
										nodo: nodo,
										dato: dato
									};
								}
								return false;
							}
						}
					}
					if (dpeuID) {
						prb.pruebaDepurada[dpeuID][ajaY ? 'success' : 'fail'] = {
							id: len - 1,
							voidd: prba,
							nodo: nodo,
							dato: dato
						};
					}
					return ajaY;
				};
			};
            
           
            
            
			PrbaLazndo.OR = function () {
				return this.obtenPrbStar(arguments, false);
			};
            
			
            
			
            
            
			PrbaLazndo.CAS_hijos = function (prba, hijosMaximos, guardaRefNombres) {
				return function (nodo, dato) {
					'!name:CAS_hijos!';
					if (prba(nodo, dato)) {
						return true;
					}
					var nodos = [];
					if (nodo.childNodes && nodo.childNodes.length) {
						nodos.push(nodo.childNodes[0]);
					}
					while (nodos.length) {
						var hijo = nodos.pop();
						if (prba(hijo, {})) {
							if (guardaRefNombres != null && dato != null) {
								dato[guardaRefNombres] = currentNode;
							}
							return true;
						}
						if (hijo.childNodes && hijo.childNodes.length && nodos.length < hijosMaximos) {
							if (hijo.nextSibling) {
								nodos.push(hijo.nextSibling);
							}
							nodos.push(hijo.childNodes[0]);
						}
						else {
							if (hijo.nextSibling) {
								nodos.push(hijo.nextSibling);
							}
						}
					}
					return false;
				};
			};
            
            
            
			PrbaLazndo.NOT = function (test) {
				return function (node, data) {
					'!name:NOT!';
					return !test(node, data);
				};
			};
            
             
			PrbaLazndo.asingnnaYA = function (nodo, dato, origen) {
				if (!origen) {
					origen = 'text';
				}
				if (typeof dato[origen] !== 'undefined') {
					return;
				}
				if (origen === 'text') {
					dato.text = (nodo.alt || nodo.textContent || nodo.innerText || nodo.value || nodo.title || nodo['node' + 'Value'] || '').replace(/\s+/ig, ' ');
				}
				else if (origen === 'html') {
					dato.html = (nodo ? nodo.innerHTML || '' : '').replace(/\s+/ig, ' ');
				}
			};
            
            PrbaLazndo.textoContipoYa = function () {
			
				return function (nodo, dato) {
					'!name:textoContipoYa!';
			
					if (nodo.nodeType === 3) {
						return true;
					}
					var he = nodo.childNodes,
						lon, q;
			
					if (he && (lon = he.length)) {
						for (q = 0; q < lon; ++q) {
			
							if (he[q].nodeType === 1) {
			
								return false;
							}
						}
					}
			
					return false;
				};
			};
            
            
			PrbaLazndo.textoRegYa = function (expresion, origen, asgnData) {
				if (!origen) {
					origen = 'text';
				}
				var prb = this;
				return function (nodo, dato) {
					'!name:textoRegYa!';
					prb.asingnnaYA(nodo, dato, origen);
					expresion.lastIndex = 0;
					var srcIndex = (dato && dato[origen] || '').search(expresion);
					if (asgnData != null) {
						if (!dato[asgnData]) {
							dato[asgnData] = [];
						}
						dato[asgnData].push(srcIndex);
					}
					var retVal = (srcIndex >= 0) ? true : false;
					return retVal;
				};
			};
            
            PrbaLazndo.AND = function () {
				return this.obtenPrbStar(arguments, true);
			};
            
			PrbaLazndo.textoRegIgualYa = function (expresion, origen) {
				if (!origen) {
					origen = 'text';
				}
				var prb = this;
				return function (nodo, dato) {
					'!name:getRegEqualText!';
					prb.asingnnaYA(nodo, dato, origen);
					expresion.lastIndex = 0;
					var w = dato && dato[origen] || '',
						h = w.match(expresion);
					if (h && h[0] === w) {
						return true;
					}
					return false;
				};
			};
            
            PrbaLazndo.vinculoYa = function (expresion, asgnData) {
			
				var propiedades = ['href', 'action', 'src'];
				return function (nodo, dato) {
					'!name:vinculoYa!';
			
					if (nodo && nodo.nodeType == 3) {
						nodo = nodo.parentNode;
					}
					if (nodo) {
						var b, e, t = 0,
							largo = propiedades.length, h;
						for (; t < largo; ++t) {
							b = propiedades[t];
							e = nodo[b];
			
							if (e && e.match) {
			
								expresion.lastIndex = 0;
								h = e.match(expresion);
			
								if (h) {
			
									if (setData != null) {
										if (!dato[asgnData]) {
											dato[asgnData] = [];
										}
										dato[asgnData].push(h[0]);
									}
									return true;
								}
							}
						}
					}
			
					return false;
				};
			};
			
            
            
			PrbaLazndo.propriedadesYa = function (expresion, propiedades, asgnData) {
				if (!propiedades) {
					propiedades = ['id', 'className', 'href', 'action', 'src'];
				}
				else if (esCadena(propiedades)) {
					propiedades = [propiedades];
				}
				return function (nodo, dato) {
					'!name:propriedadesYa!';
				if (nodo && nodo.nodeType == 3) {
						nodo = nodo.parentNode;
					}
					if (nodo) {
						var b, e, t = 0,
							Long = propiedades.length;
						for (; t < Long; ++t) {
							b = propiedades[t];
							try {
								e = nodo[b];
							}
							catch (ex) {
								console.log('propriedadesYa error: ', ex);
							}
							finally {
								if (b === 'className' || b === 'href' || b === 'value' || b === 'checked' || b === 'tagName' || b === 'nodeName' || b === 'nodeType' || b === 'nodeValue') {
									e = nodo[b];
								}
								else {
									e = (nodo.getAttribute && nodo.getAttribute(b)) || null;
								}
							}
							
							if (e && e.search) {
								var bscaIndYa = -1;
							
								expresion.lastIndex = 0;
							
								if (b === 'href' || b === 'src' || b === 'action') {
							
									bscaIndYa = e.replace(/^http(s)*:\/\/.*?\//, '').search(expresion);
								}
								else {
									bscaIndYa = e.search(expresion);
								}
							
								if (bscaIndYa >= 0) {
							
									if ((b == 'href') && (nodo.href.indexOf(document.URL) >= 0)) {
							
										return false;
									}
							
									if (asgnData != null) {
										if (!dato[asgnData]) {
											dato[asgnData] = [];
										}
										dato[asgnData].push(bscaIndYa);
									}
									return true;
								}
							}
						}
					}
					
					return false;
				};
			};
			
            PrbaLazndo.buscaCssYa = function (estilo, expresion) {
				if (esCadena(estilo)) {
					estilo = [estilo];
				}
				return function (nodo, dato) {
					'!name:buscaCssYa!';
					if (nodo.nodeType === 3) {
						nodo = nodo.parentNode;
					}
					var evaluaRet = true;
					if (!nodo.nodeName || nodo.nodeName === 'HTML') {
						evaluaRet = false;
					}
					else {
						!dato.css && (dato.css = {});
						dato.css = $.extend({}, dato.css, $(nodo).css(estilo));
						$.each(dato.css, function (j, x) {
							expresion.lastIndex = 0;
							if (!expresion.test(x)) {
								evaluaRet = false;
								return false;
							}
						});
					}
					return evaluaRet;
				};
			};
			
			PrbaLazndo.largodeTextoYa = function (largoMaximo, origen) {
				if (!origen) {
					origen = 'text';
				}
				var prb = this;
			
				return function (nodo, dato) {
					'!name:largodeTextoYa!';
			
					prb.asingnnaYA(nodo, dato, origen);
					var rsultdoListo = Boolean(dato && dato[origen] && (dato[origen].length <= largoMaximo));
					
					return rsultdoListo;
				};
			};
			
			PrbaLazndo.prgVisibleYa = function () {
			
				return function (nodo, dato) {
					'!name:prgVisibleYa!';
					if (nodo.nodeType === 3) {
						nodo = nodo.parentNode;
					}
					if (nodo.nodeName && /img/i.test(nodo.nodeName) && !nodo.complete) {
						return true;
					}
					var lsito = (nodo.offsetWidth && nodo.offsetHeight && (nodo.offsetWidth > 2 || nodo.offsetHeight > 2)) ? true : false;
			
					return lsito;
				};
			};
			
            
			PrbaLazndo.propiedadesUtilYa = function (prba, propiedades, igual) {
			
				return function (nodo, dato) {
					'!name:propiedadesUtilYa!';
					var rsultdoListo = prba(nodo, dato) && ((Math.max.apply(Math, dato[propiedades]) - Math.min.apply(Math, dato[propiedades])) < igual);
			
					return rsultdoListo;
				};
			};
            
			
			
			PrbaLazndo.tendraAtribYa = function (propiedades) {
			
				return function (nodo, dato) {
					'!name:tendraAtribYa!';
					var termino = Boolean((nodo.wrappedJSObject) ? nodo.wrappedJSObject[propiedades] : nodo[propiedades]);
			
					return termino;
				};
			};
			
			PrbaLazndo.limitnaYa = function (arribaMin, arribaMax, anchoMin, anchoMax) {
			
				return function (nodo, dato) {
					'!name:limitnaYa!';
					if (nodo.nodeType === 3) {
						nodo = nodo.parentNode;
					}
					var offsetY = nodo.offsetTop;
					var offsetX = nodo.offsetLeft;
					while (nodo.offsetParent) {
						nodo = nodo.offsetParent;
						offsetY += nodo.offsetTop;
						offsetX += nodo.offsetLeft;
					}
					var eclaListo = !((offsetY < arribaMin) || (offsetY >= arribaMax) || (offsetX < anchoMin) || (offsetX >= anchoMax));
			
					return eclaListo;
				};
			};
			
			PrbaLazndo.resultadoBscYa = function (expresiones) {
			
				return function () {
					'!name:resultadoBscYa!';
					var terminado = expresiones.test( document.domain) ? true : false;
			
					return terminado;
				};
			};
			
            
			PrbaLazndo.navMediLargaYa = function () {
				var prb = this;
				return function (nodo, dato) {
					'!name:navMediLargaYa!';
					return prb.OR(prb.CAS_pasdres(prb.propriedadesYa(/susmenu|rcm|seemore|header|bread[\-_]?crumb|heading|ad_|\bad\b|\bnav(?:iga|\-|_)|top[\s\-_]*nav/i, ['id', 'className']), 15),
			
						prb.CAS_pasdres(prb.propriedadesYa(/menu/i, ['id', 'className']), 3), prb.CAS_pasdres(prb.propriedadesYa(/\bheader\b|\bnav\b/i, ['nodeName']), 9))(nodo, dato);
				};
			};
			
			PrbaLazndo.conClickYa = function () {
				var prb = this,
					pruebaClickHijosNodo = prb.OR(prb.propriedadesYa(/^input|button|a$/i, ['nodeName']), prb.tendraAtribYa('onclick'));
				return function (nodo, dato) {
					'!name:conClickYa!';
					return prb.OR(prb.buscaCssYa('cursor', /pointer/i), pruebaClickHijosNodo, prb.CAS_hijos(pruebaClickHijosNodo), prb.CAS_pasdres(pruebaClickHijosNodo, 4))(nodo, dato);
				};
			};
            
			PrbaLazndo.listandoYa = function () {
				var prb = this;
				return function (nodo, dato) {
					'!name:listandoYa!';
					!dato && (dato = {});
					var estaLista = prb.OR(prb.CAS_pasdres(prb.propriedadesYa(reLiTag, ['nodeName']), 7, 'listNode'), prb.CAS_pasdres(tst.propriedadesYa(reListing, ['className', 'id']), 5, 'listNode'))(nodo, dato);
			
					if (estaLista && dato && dato.listNode) {
						var bein = $(dato.listNode).siblings();
						if (bein.length) {
							var sumptb = NodosComparados(dato.listNode, bein[0], false, true);
							if (sumptb >= 85) {
								return true;
							}
							return false;
						}
					}
					return false;
				};
			};
			            
			var prbaEnPLeno = new Probador(),
				prbBaskCuerpo, prbaPagiYaCmplta;
			
			function prbaCrpoYa(reNueva) {
				if (!prbBaskCuerpo || reNueva) {
					prbBaskCuerpo = prbaEnPLeno.empzDepu('OR', [
						prbaEnPLeno.AND(prbaEnPLeno.textoRegYa(rAgr, 'html'), prbaEnPLeno.textoRegYa(rCarro, 'html')),
						prbaEnPLeno.AND(prbaEnPLeno.textoRegYa(rComp, 'html'), prbaEnPLeno.textoRegYa(rFComp, 'html')),
						prbaEnPLeno.textoRegYa(rPrOrd, 'html'),
						prbaEnPLeno.textoRegYa(rAgr2Carro, 'html')
					], ['body-test-add+cart-kw', 'body-test-buy+now-kw', 'body-test-preorder-kw', 'body-test-addToCart-kw']);
				}
				return prbBaskCuerpo;
			}
			
			function prbaPgnaYaCompleta(reNueva) {
				if (!prbaPagiYaCmplta || reNueva) {
					var prbaAttrya = ['className', 'id', 'title', 'name', 'alt', 'value', 'src', 'href', 'onclick'],
						prbaStilaAtras = prbaEnPLeno.OR(prbaEnPLeno.buscaCssYa('background', rComp), prbaEnPLeno.buscaCssYa('background', rAgr));
					var alltests = [
			
						prbaEnPLeno.OR(prbaEnPLeno.textoContipoYa(), prbaEnPLeno.textoRegYa(rAgr2Carro), prbaEnPLeno.propriedadesYa(rAgr2Carro, prbaAttrya), prbaEnPLeno.AND(prbaEnPLeno.textoRegYa(rComp), prbaEnPLeno.OR(prbaEnPLeno.propriedadesYa(rComp, prbaAttrya), prbaEnPLeno.propriedadesYa(rCarro, prbaAttrya), prbaEnPLeno.propriedadesYa(rAgr2Carro, prbaAttrya))),
			
							prbaStilaAtras,
			
							prbaEnPLeno.AND(prbaEnPLeno.propriedadesYa(/\b(?:a|input|button)\b/i, ['nodeName']), prbaEnPLeno.OR(prbaEnPLeno.propriedadesYa(rAgr, ['textContent', 'innerText', 'value']), prbaEnPLeno.propriedadesYa(rComp, ['textContent', 'innerText', 'value']), prbaEnPLeno.propriedadesYa(rComYa, ['textContent', 'innerText', 'value'])))),
			
						prbaEnPLeno.prgVisibleYa(),
			
						prbaEnPLeno.OR(
			
							prbaEnPLeno.propriedadesYa(rAgr2Carro, ['id', 'className']),
			
							prbaEnPLeno.AND(prbaEnPLeno.textoRegYa(rAgr), prbaEnPLeno.textoRegYa(rCarro), prbaEnPLeno.largodeTextoYa(30)),
			
							prbaEnPLeno.AND(prbaEnPLeno.textoRegYa(rComp), prbaEnPLeno.textoRegYa(rFComp), prbaEnPLeno.largodeTextoYa(20)),
			
							prbaEnPLeno.AND(prbaEnPLeno.textoRegYa(rPrOrd), prbaEnPLeno.largodeTextoYa(16)), prbaEnPLeno.propiedadesUtilYa(prbaEnPLeno.AND(prbaEnPLeno.OR(prbaEnPLeno.textoRegYa(rComp, null, 'index-solo-buy')), prbaEnPLeno.OR(prbaEnPLeno.propriedadesYa(rComp, prbaAttrya, 'index-solo-buy'), prbaEnPLeno.propriedadesYa(rCarro, prbaAttrya, 'index-solo-buy'), prbaEnPLeno.propriedadesYa(rAgr2Carro, prbaAttrya, 'index-solo-buy'), prbaEnPLeno.propriedadesYa(rComYa, prbaAttrya, 'index-solo-buy'))), 'index-solo-buy', 10),
			
							prbaEnPLeno.propiedadesUtilYa(prbaEnPLeno.AND(prbaEnPLeno.propriedadesYa(rComp, null, 'index-buy'), prbaEnPLeno.propriedadesYa(rFComp, null, 'index-buy')), 'index-buy', 10),
			
							prbaEnPLeno.propiedadesUtilYa(prbaEnPLeno.AND(prbaEnPLeno.propriedadesYa(rAgr, null, 'index-add'), prbaEnPLeno.propriedadesYa(rCarro, null, 'index-add')), 'index-add', 10),
			
							prbaEnPLeno.propriedadesYa(rPrOrd),
			
							prbaEnPLeno.vinculoYa(rAgr2Carro),
			
							prbaStilaAtras,
			
							prbaEnPLeno.textoRegIgualYa(rAgr)),
			
						prbaEnPLeno.OR(prbaEnPLeno.limitnaYa(0, 900), prbaEnPLeno.resultadoBscYa(/\.jp$/)),
			
						prbaEnPLeno.NOT(prbaEnPLeno.navMediLargaYa()),
			
						prbaEnPLeno.conClickYa(),
			
						prbaEnPLeno.NOT(prbaEnPLeno.CAS_pasdres(prbaEnPLeno.propriedadesYa(/map|area/i, ['nodeName']), 4)),
			
						prbaEnPLeno.OR(prbaEnPLeno.AND(prbaEnPLeno.limitnaYa(0, 150), prbaEnPLeno.NOT(prbaEnPLeno.listandoYa())), prbaEnPLeno.limitnaYa(0, 2000))
					];
					prbaPagiYaCmplta = prbaEnPLeno.empzDepu('AND', alltests, ['text-OR-addToCart', 'visibility-check', 'OR-props', 'bounds-check', 'nav-check', 'clickable-check', 'NOT-map|area', 'NOT-in-listing']);
				}
				return prbaPagiYaCmplta;
			}
            
			var conHerencia = function (ofortuno, afortunado) {
					var nod = afortunado.parentNode;
					while (nod != null) {
						if (nod === ofortuno) {
							return true;
						}
						nod = nod.parentNode;
					}
					return false;
				},
                
			caminaArriAbjoDocm = function (origen, queMuestro) {
					var sinEspacios = /^\s*$/,
						noseScrpt = /script|style/i;
					return document.createTreeWalker(origen, queMuestro, function (nod) {
						var nTienELm = (nod.nodeType == 1) ? true : false,
							nmbrNod = nTienELm ? nod.nodeName.toLowerCase() : '',
							ofortuno = nod.parentNode;
				
						if (
							(nTienELm && noseScrpt.test(nmbrNod)) || (ofortuno && noseScrpt.test(ofortuno.nodeName))) {
							return window.NodeFilter.FILTER_REJECT;
						}
				
						else {
				
							if (nTienELm) {
								return window.NodeFilter.FILTER_ACCEPT;
							}
				
							else {
								if (!sinEspacios.test(nod.data)) {
									return window.NodeFilter.FILTER_ACCEPT;
								}
							}
						}
						return window.NodeFilter.FILTER_REJECT;
					}, false);
				}
			
			
			function prbaDescalificadaYa(puntos) {
				var t = 0,
					lon = puntos.length;
				
				if (lon == 1) {
					return true;
				}
				else if (lon < 1) {
					return false;
				}
				else
				
				if (lon > 1) {
					var vti = [],
						actual = puntos[0],
						sig, vtiLon = 0;
					vti.push(actual);
					for (var d = 1, la = puntos.length; d < la; ++d) {
						sig = puntos[d];
				
						if (actual === sig) {
							continue;
						}
						else if (!conHerencia(actual, sig)) {
							actual = sig;
							vti.push(actual);
						}
					}
					
					vtiLon = vti.length;
					
					if (vtiLon == 1) {
						return true;
					}
					else if (vtiLon > 1) {
					
						if (vtiLon === 2) {
					
							if (NodosComparados(btn[0], vti[1], true)) {
								
					
								return false;
							}
					
							else {
								return true;
							}
						}
					
						else {
							return false;
						}
					}
					else {
						return false;
					}
				}
			}
			NodosComparados = function(el1, el2, hondo, returnPuntos) {
                var puntaje = 100,
                    letrs1 = String(el1.textContent || el1.innerText),
                    letrs2 = String(el2.textContent || el2.innerText);
                if (letrs1 !== letrs2) {
                    if (!returnPuntos) {
                        return false;
                    }
                    puntaje -= Math.abs(letrs1.length - letrs2.length) / 4;
                }
                if (el1.nodeType !== el2.nodeType) {
                    if (!returnPuntos) {
                        return false;
                    }
                    puntaje -= 15;
                }
                if (el1.nodeType != 3 && (el1.nodeName !== el2.nodeName)) {
                    if (!returnPuntos) {
                        return false;
                    }
                    puntaje -= 15;
                }
                var el14Long = el1.childNodes.length,
                    el24Long = el2.childNodes.length;
                if (el14Long !== el24Long) {
                    if (!returnPuntos) {
                        return false;
                    }
                    puntaje -= 5 * (Math.abs(el14Long - el24Long) / 2);
                }
                if (el1.nodeType == 3) {
                    el1 = el1.parentNode;
                }
                if (el2.nodeType == 3) {
                    el2 = el2.parentNode;
                }
                var stilos = ['display', 'fontSize', 'fontWeight', 'fontStyle', 'fontFamily', 'textDecoration', 'cursor', 'textAlign', 'padding', 'color', 'border', 'background', 'height', 'lineHeight'],
                    css1 = $(el1).css(stilos),
                    css2 = $(el2).css(stilos),
                    i = 0,
                    l = stilos.length,
                    s;
                for (; i < l; ++i) {
                    s = stilos[s];
                    if (css1[s] !== css2[s]) {
                        if (!returnPuntos) {
                            return false;
                        }
                        puntaje -= 5;
                    }
                }
                var retVal = true;
                if (hondo && el14Long && (el14Long === el24Long)) {
                    for (i = 0; i < el14Long; ++i) {
                        retVal = NodosComparados(el1.childNodes[i], el2.childNodes[i], hondo, returnPuntos);
                        if (!retVal || returnPuntos) {
                            if (!returnPuntos) {
                                return false;
                            }
                            puntaje = puntaje - (100 - retVal);
                        }
                    }
                }
                return (returnPuntos ? puntaje : retVal);
            }
			
			function detectarSinPathnameTest(){
                var nodo_product = $('*').items('http://schema.org/Product');
                if(nodo_product && nodo_product.length > 0){
                    return true;
                }else {
                    if( !location && !location.pathname ){
                        return false;
                    }else if(prbaCrpoYa()(document.body, {})){
                        var domWalker = caminaArriAbjoDocm(document.body, window.NodeFilter.SHOW_ELEMENT | window.NodeFilter.SHOW_TEXT),
						pageTests = prbaPgnaYaCompleta(),
						validNodes = [],
						nodesWalked = 0,
						maxNodes = 0,
                        retorno = false;
                        
                        var continueWalkingNodes = function (onCompleteTest) {
							var node;
					
							for (var i = 0; i < 30; ++i) {
					
								node = domWalker.nextNode();
					
								if (!node) {
									onCompleteTest();
									return;
								}
								nodesWalked++;
					
								if (pageTests(node, {})) {
									validNodes.push(node);
								}
								
					
								if (maxNodes && (nodesWalked >= maxNodes)) {
									onCompleteTest();
									return;
								}
							}
					
							
								continueWalkingNodes(onCompleteTest);
							
						},
						afterWalkFinished = function (){
							for (var i = 0; i < validNodes.length; i++) {
								if (validNodes[i].nodeType == 3) {
									validNodes[i] = validNodes[i].parentNode;
								}
							}
							var isNotListing = prbaDescalificadaYa(validNodes);
//                             alert("Ahi vamos otra vez!: "+isNotListing);
                            if (isNotListing) {
//                                 alert("Colocando retorno true");
								retorno = true;
							}else{
//                                 alert("Escuchador dio FALSE");
                                retorno = false;
                            }
						}
					
					continueWalkingNodes(afterWalkFinished);
					    
                        //alert("Este es el retorno: "+retorno);
                        return retorno;
                    
                    }else {
//                         alert("Salida 2");
                        return false;
                    }
                }
//                 alert("Salida 3");
                return false;
            }

            function hideFrame(){
                var fr = document.getElementById("mimarco");
                fr.style.display="none";
            } 


chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {
        if(request.method == "getText"){
            var nodoT, nodoP, microdata_result =microdataMethod();
            
            if(microdata_result && microdata_result.puntos &&  microdata_result.puntos> 0){
                nodoT = microdata_result;
                if(resultados_price.length < 1 ){
                    var pintPrice = document.querySelectorAll("[class*='price']");
                    sinPrecio(pintPrice);
                    resultados_price.sort(function (a, b) {
                        if(a.puntos_css > 0 ){
                            a.puntos = a.puntos + a.puntos_css;
                        }else if(b.puntos_css > 0){
                            b.puntos = b.puntos + b.puntos_css;
                        }
                        if (a.puntos > b.puntos) {
                            return -1;
                        }
                        else if (a.puntos < b.puntos) {
                            return 1;
                        }
                        else {
                            return 0;
                        }
					});
                }
                nodoP = resultados_price[0];
            }
            else{
                nodoT = detectonMethod()[0];
//                   alert("nodo name: "+nodoT.el.nodeName+" Puntaje: "+   iterateWalkEnd(nodoT.el));
//                   alert("nodo name: "+nodoT.puntos);
                var nodo_principal =  nodoT.el, i=0, tmpV = iterateWalkEnd(nodoT.el)/10;
                    
                for(;i<10;i++){
                    if(nodo_principal.tagName.toLowerCase()=="body"){break;}
                    nodo_principal = nodo_principal.parentNode;
                }
                burcarNodosPrecioXhijos(nodo_principal); 
                for(var ii=0; ii < resultados_price.length; ii++){
                    resultados_price[ii].puntos = resultados_price[ii].puntos + porcentajeXprecio(tmpV, resultados_price[ii].puntos);
                }
                               
                resultados_price.sort(function (a, b) {
                                if(a.puntos_css > 0 ){
                                    a.puntos = a.puntos + a.puntos_css;
                                }else if(b.puntos_css > 0){
                                    b.puntos = b.puntos + b.puntos_css;
                                }
								if (a.puntos > b.puntos) {
									return -1;
								}
								else if (a.puntos < b.puntos) {
									return 1;
								}
								else {
									return 0;
								}
							});
                if(resultados_price.length < 1){
                    var pintPrice = document.getElementsByClassName('price')[0];
                    resultados_price.push({el: pintPrice, moneda: pintPrice.innerText.match(/(\$)|(\€)|(\£)/gi), precio: pintPrice.innerText.match(/((?:\d|(\.|\,))*(\.|,)?\d+)/gi), puntos_css: 0,  puntos: (iterateWalkEnd(pintPrice)/10)});
                }
                nodoP = resultados_price[0];
            }
            
            if(/(\,)/g.test(nodoP.precio)){
                nodoP.precio = parseFloat(nodoP.precio.toString().replace(',','.').replace(' ',''));
            }
        
//             alert("Gano precio: "+ nodoP.precio + " Con: " + nodoP.puntos + "  Puntos");
                
            sendResponse({titulo_t: (nodoT && nodoT.puntos) ? nodoT.valor : 'NO', precio_p: nodoP.precio, moneda_m: nodoP.moneda,  method: "getText"}); 
        }
        else if(request.method == 'detectionWhitoutPagePathname'){
            var test =  detectarSinPathnameTest() ? detectarSinPathnameTest()  : null;
            sendResponse({data: test ? true : false , method: "detectionWhitoutPagePathname"}); 
        }
        else if (request.method == 'closeFrame') {
            document.getElementById("mimarco").remove();
        }
    });
